package com.example.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServerApplicationTests {

	@Test
	private String [] processInput(String input) {
		String[] in = input.split(" ");

		for (int i = 0; i < in.length; i++) {

			in[i].replaceAll(","," ");
			in[i].replaceAll("."," ");
			in[i].trim();
		}
		return in;
	}

	@Test
	public void contextLoads() {
	}

}
