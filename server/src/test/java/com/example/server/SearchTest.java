package com.example.server;

import com.example.server.recipe.Ingredient;
import com.example.server.recipe.Search;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;

public class SearchTest {

    Search search = new Search();
    @Test
    public void processInput() {

        String eingabe = "Kartoofel, Lachs, Mayo";
        String[] result = search.processInput(eingabe);
        assertArrayEquals(new String[]{"Kartoofel", "Lachs", "Mayo"}, result);


        String eingabe1 = "Salz, Milch, Reis...";
        String[] result1 = search.processInput(eingabe1);
        assertArrayEquals(new String[]{"Salz", "Milch", "Reis"}, result1);


        String eingabe2 = "suppe, Leber, knödel";
        String[] result2 = search.processInput(eingabe2);
        assertArrayEquals(new String[]{"suppe", "Leber", "knödel"}, result2);
    }


}