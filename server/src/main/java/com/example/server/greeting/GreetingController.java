package com.example.server.greeting;
import com.example.server.api.SecurityService;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class GreetingController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GreetingRepository greetingRepository;

    @Autowired
    private SecurityService securityService;

    @PostMapping("/login")
    public Greeting create() {
        Greeting greeting = new Greeting();
        greeting.setUser(securityService.getSessionUser().orElse(null));
        return greetingRepository.save(greeting);
    }
}
