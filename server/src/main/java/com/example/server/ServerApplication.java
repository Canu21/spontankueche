package com.example.server;

import com.example.server.security.WebSecurityConfiguration;
import com.example.server.user.User;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@SpringBootApplication
@Import(WebSecurityConfiguration.class)
public class ServerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
//        if (!userRepository.findByUsername("test").isPresent()) {
//            userRepository.save(new User("test", passwordEncoder.encode("test123"), "hi@asdshi.de"));
//        }
//
//        if (!userRepository.findByUsername("test2").isPresent()) {
//            userRepository.save(new User("test2", passwordEncoder.encode("test123"), "hi@hi.de"));
//        }
//        if (!userRepository.findByUsername("admin").isPresent()) {
//            User admin = new User("admin", passwordEncoder.encode("admin"), "blxxxa@bla.de");
//            userRepository.save(admin);
//        }
    }
}
