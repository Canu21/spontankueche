package com.example.server.images;

import com.example.server.api.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ImageController {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    SecurityService securityService;

    @PostMapping("/saveImage")
    public void saveImage(@RequestBody ImageModel imageModel) {
        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }
        Image image = toImageEntity(imageModel);
        imageRepository.save(image);
    }

    @GetMapping("/readAllImages")
    public List<ImageModel> readAllImages() {
        List<Image> images = imageRepository.findAll();

        List<ImageModel> models = new ArrayList<>();
        for (Image image : images) {
            ImageModel model = toImageModel(image);
            models.add(model);
        }
        return models;
    }

    private Image toImageEntity(@RequestBody ImageModel imageModel) {
        Image image = new Image();
        image.setName(imageModel.getName());
        image.setContentType(imageModel.getContentType());
        image.setData(imageModel.getData());
        return image;
    }

    private ImageModel toImageModel(Image image) {
        ImageModel model = new ImageModel();
        model.setName(image.getName());
        model.setContentType(image.getContentType());
        model.setData(image.getData());
        return model;
    }
}
