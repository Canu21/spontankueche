package com.example.server.images;


public class ImageModel {

    private String imagename;
    private String contentType;
    /** Image as binary string */
    private String data;

    private Long recipeId;


    public String getName() {
        return imagename;
    }

    public void setName(String name) {
        this.imagename = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}