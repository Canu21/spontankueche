package com.example.server.comment;

import com.example.server.api.SecurityService;
import com.example.server.recipe.Recipe;
import com.example.server.recipe.RecipeRepository;
import com.example.server.recipe.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    SecurityService securityService;

    @GetMapping(value = "/recipes/{recipeId}/comments/desc")
    public Page<Comment> getAllCommentsByPostIdByOrderByCreateDateDesc(@PathVariable(value = "recipeId") Long recipeId, Pageable pageable) {
        return commentRepository.findByRecipeIdOrderByCreateDateDesc(recipeId, pageable);
    }

    @GetMapping(value = "/recipes/{recipeId}/comments/asc")
    public Page<Comment> getAllCommentsByPostIdByOrderByCreateDateAdc(@PathVariable(value = "recipeId") Long recipeId, Pageable pageable) {
        return commentRepository.findByRecipeIdOrderByCreateDateAsc(recipeId, pageable);
    }

    @GetMapping(value = "/recipes/{recipeId}/comments")
    public Page<Comment> getAllCommentsByPostId(@PathVariable(value = "recipeId") Long recipeId, Pageable pageable) {
        return commentRepository.findByRecipeId(recipeId, pageable);
    }

    @PostMapping(value = "/recipes/{recipeId}/comments")
    public Comment createComment(@RequestBody Comment comment, @PathVariable Long recipeId) {
        Recipe recipe = recipeRepository.findById(recipeId).
                orElseThrow(() -> new IllegalArgumentException("Recipe not found"));

        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        } else {

            comment.setRecipe(recipe);
            comment.setUser(securityService.getSessionUser().orElse(null));
            return commentRepository.save(comment);
        }
    }

    @PutMapping(value = "/recipes/{recipeId}/comments/{commentId}")
    public Comment updateComment(@PathVariable(value = "commentId") Long commentId,
                                 @RequestBody CommentDTO commentRequest) {

        Comment comment = commentRepository.findById(commentId).
                orElseThrow(() -> new IllegalArgumentException("Comment not found"));

        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }

        if (!securityService.getSessionUser().get().getUsername().equals(comment.getUser().getUsername())) {
            throw new UnauthorizedException("User has no permission");
        } else {

            comment.setText(commentRequest.text);
            return commentRepository.save(comment);
        }
    }

    @DeleteMapping(value = "/recipes/{recipeId}/comments/{commentId}")
    public ResponseEntity<?> deleteComment(@PathVariable(value = "commentId") Long commentId) {
        Comment comment = commentRepository.findById(commentId).
                orElseThrow(() -> new IllegalArgumentException("Comment not found"));

        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }

        if (!securityService.getSessionUser().get().getUsername().equals(comment.getUser().getUsername())) {
            throw new UnauthorizedException("User has no permission");
        } else {

            commentRepository.delete(comment);
            return ResponseEntity.ok().build();
        }

    }
}
