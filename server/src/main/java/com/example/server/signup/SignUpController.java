package com.example.server.signup;

import com.example.server.api.SecurityService;
import com.example.server.user.User;
import com.example.server.user.UserDTO;
import com.example.server.user.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class SignUpController {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityService securityService;


    @PostMapping("/signup")
    public User signUp(@RequestBody UserDTO accountDto) {

        User user = new User();

        user.setUsername(accountDto.getUsername());
        user.setUserEmail(accountDto.getUserEmail());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));

        return userRepository.save(user);

    }

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
