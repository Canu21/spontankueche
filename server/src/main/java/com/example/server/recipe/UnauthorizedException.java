package com.example.server.recipe;

import org.hibernate.service.spi.ServiceException;

public class UnauthorizedException extends ServiceException {

    public UnauthorizedException(String message) {
        super(message);
    }
}
