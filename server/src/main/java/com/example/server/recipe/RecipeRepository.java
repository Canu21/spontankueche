package com.example.server.recipe;


import com.example.server.ingredient.Ingredient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    List<Recipe> findByIngredientsIn(List<Ingredient> ingredients);

    Page<Recipe> findByIngredients_Name(String name, Pageable pageable);

    Page<Recipe> findAllByOrderByCreateDateDesc(Pageable pageable);

    Page<Recipe> findAllByOrderByCreateDateAsc(Pageable pageable);

}
