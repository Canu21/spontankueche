package com.example.server.recipe;
import com.example.server.api.SecurityService;
import com.example.server.ingredient.Ingredient;
import com.example.server.ingredient.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class RecipeController {

    @Autowired
    IngredientRepository ingredientRepository;

    @Autowired
    RecipeService recipeService;

    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/recipes/asc")
    public Page<Recipe> getAllRecipesByAsc(Pageable pageable) {
        return recipeRepository.findAllByOrderByCreateDateAsc(pageable);
    }

    @GetMapping("/recipes/desc")
    public Page<Recipe> getAllRecipesByDesc(Pageable pageable) {
        return recipeRepository.findAllByOrderByCreateDateDesc(pageable);
    }

    @GetMapping("/recipes")
    public Page<Recipe> getAllRecipes(Pageable pageable) {
        return recipeRepository.findAll(pageable);
    }

    @GetMapping("/recipe/{recipeId}")
    public Optional<Recipe> getRecipe(@PathVariable Long recipeId) throws Exception {
        return recipeRepository.findById(recipeId);
    }

    @GetMapping("/recipes/{ingredients}")
    public SearchResult getAllRecipesByIngredient(@PathVariable String ingredients) throws Exception {
        List<Ingredient> ingredientList = new LinkedList<>();
        for (String name : ingredients.split(" ")) {
            List<Ingredient> i = ingredientRepository.findByName(name);

            if (i.size() == 0) {
                throw new Exception("Kein Rezept mit den Zutaten gefunden");
            }

            ingredientList.addAll(i);
        }

        SearchResult result = new SearchResult();
        result.otherResults = recipeRepository.findByIngredientsIn(ingredientList);

        for (Recipe recipe : result.otherResults) {

            if (ingredientList.containsAll(recipe.getIngredients())) {
                result.topResults.add(recipe);
            }

        }
        result.otherResults.removeAll(result.topResults);

        return result;
    }
    int recipeid;
    @PostMapping("/createrecipe")
    public Recipe createRecipe(@RequestBody Recipe recipe) {
        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }
        recipe.setUser(securityService.getSessionUser().orElse(null));

        return recipeService.saveRecipe(recipe);
    }

    @GetMapping("/getrecipeid")
    public long idForImage (Recipe recipe) throws Exception{
            List<Recipe> recipeList = new ArrayList<>();
            recipeList = recipeRepository.findAll();
            long id = recipeList.size();
            return id;
    }

    @DeleteMapping("/recipes/{recipeId}")
    public ResponseEntity<?> deleteRecipe(@PathVariable Long recipeId) {

        Recipe recipe = recipeRepository.findById(recipeId).
                orElseThrow(() -> new IllegalArgumentException("Recipe not found"));

        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }

        if (!securityService.getSessionUser().get().getUsername().equals(recipe.getUser().getUsername())) {
            throw new UnauthorizedException("User has no permission");
        } else {

            recipeRepository.delete(recipe);
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping("/recipes/{recipeId}")
    public Recipe updateRecipe(@RequestBody RecipeDTO recipeRequest, @PathVariable Long recipeId) {

        Recipe recipe = recipeRepository.findById(recipeId).
                orElseThrow(() -> new IllegalArgumentException("Recipe not found"));

        if (!securityService.getSessionUser().isPresent()) {
            throw new UsernameNotFoundException("User in not logged in");
        }

        if (!securityService.getSessionUser().get().getUsername().equals(recipe.getUser().getUsername())) {
            throw new UnauthorizedException("User has no permission");
        } else {

            recipe.setTitle(recipeRequest.title);
            recipe.setInstruction(recipeRequest.instruction);

            return recipeRepository.save(recipe);
        }
    }
}