package com.example.server.recipe;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;


public class Category {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Map zutaten;


    public Category(Long id, String name, Map zutaten) {
        this.id = id;
        this.name = name;
        this.zutaten = zutaten;
    }

    public Category() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map getZutaten() {
        return zutaten;
    }

    public void setZutaten(Map zutaten) {
        this.zutaten = zutaten;
    }
}
