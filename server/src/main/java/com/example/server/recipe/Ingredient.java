package com.example.server.recipe;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;
import java.util.Set;


public class Ingredient<ingredientID, name> {

    @Id
    @GeneratedValue
    private Long ingredientID;
    private String name;
    private Set<Category> catSet;
    private Map<ingredientID, name> fullIngredients;

    public Ingredient(Long ingredientID, String name, Set<Category> catSet) {
        this.ingredientID = ingredientID;
        this.name = name;
        this.catSet = catSet;
    }


    public Ingredient() {
    }


    public Map<ingredientID, name> getFullIngredients() {
        return fullIngredients;
    }

    public void setFullIngredients(Map<ingredientID, name> fullIngredients) {
        this.fullIngredients = fullIngredients;
    }

    public Long getIngredientID() {
        return ingredientID;
    }

    public void setIngredientID(Long ingredientID) {
        this.ingredientID = ingredientID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Category> getCatSet() {
        return catSet;
    }

    public void setCatSet(Set<Category> catSet) {
        this.catSet = catSet;
    }
}

