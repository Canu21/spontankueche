package com.example.server.recipe;

import java.util.LinkedList;
import java.util.List;

public class SearchResult {

    public List<Recipe> topResults = new LinkedList<>();
    public List<Recipe> otherResults = new LinkedList<>();

}
