# Spring Boot + Angular Startpack

## Download Dependencies

    cd server
    mvn dependency:resolve

    cd ../client
    npm install

## Building and running

### Production mode

For production, first compile the Angular client into the server's static resource folder:

    cd client
    ng build --prod --output-path ../server/src/main/resources/static/

Afterwards, you may compile the server:

    cd ../server
    mvn package


### Development Mode

The best developer experience is achieved by using Angular's http-server which is configured to proxy API requests to the Spring backend.

Run the server from your IDE or via `mvn spring-boot:run` (from the server directory). Pass a JDBC_DATABASE_URL environment 
variable, e.g. "jdbc:h2:mem:testdb"

Then start the client development server via `ng server` (from the client directory).