import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../service/recipe.service';
import { Recipe } from '../recipe/recipe';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  recipes: Recipe[] = [];
  recipes2: Recipe[] = [];

  recipe = {
    title: '',
    instruction: '',
    Ingredient: [],
  };
  name = '';

  constructor(private _recipeService: RecipeService) { }

  ngOnInit() {
  }

  submit3() {
    this._recipeService.search(this.name).subscribe(
      data => {
        this.recipes = data.topResults;
        this.recipes2 = data.otherResults;
      },
      error => console.log(error)
    );
  }

}
