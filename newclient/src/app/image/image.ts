export interface Image {
    name: string;
    contentType: string;
    data: string;
    recipeid?: number; 
}

