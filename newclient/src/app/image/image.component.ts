import { Component, OnInit } from '@angular/core';
import { Image } from './image';
import { Imageservice } from '../service/image.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
file: File = new File([], '');

  image = {
    name: '',
    contentType: '',
    data: '',
  };

  images: Image[] = [ { name: 'name', contentType: '', data: ''} ];

  constructor(private imageservice: Imageservice) { }

  ngOnInit() {
  }

  onFileChange(event: any) {
    console.log('onFileChange()');

    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];
      const img = document.querySelector('#preview img') as HTMLImageElement;
      // img.file = this.file;

      const reader = new FileReader();
      reader.onload = (e) => {
          img.src = reader.result.toString();
      };
      reader.readAsDataURL(this.file);
    }
  }

  onUpload() {
    console.log('onUpload()');

    this.image.name = this.file.name;
    this.image.contentType = this.file.type;
    const reader = new FileReader();
    reader.onload = (e) => {
      this.image.data = reader.result.toString();
    };
    reader.readAsDataURL(this.file);

    this.imageservice.saveImage(this.image);
  }

  onShow() {
    console.log('onShow()');
    this.imageservice.readAllImages().forEach(
      next => this.images = next
    );
  }
}


