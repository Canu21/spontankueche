import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { GreetingCreateComponent } from './greeting/greeting-create/greeting-create.component';
import { UserInfoComponent } from './security/user-info/user-info.component';
import { SignupComponent } from './signup/signup.component';
import { FoodWasteComponent } from './food-waste/food-waste.component';
import { FoodStorageComponent } from './food-storage/food-storage.component';
import { AppRoutingModule } from './app-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { FuufuuComponent } from './fuufuu/fuufuu.component';
import { ImageComponent } from './image/image.component';
import { RecipeListComponent } from './recipe/recipe-list/recipe-list.component';

import { SearchComponent } from './recipe/search/search.component';
import { CreateRecipeComponent } from './recipe/create-recipe/create-recipe.component';
import { ViewRecipeComponent } from './recipe/view-recipe/view-recipe.component';
import { RecipePreviewComponent } from './recipe/recipe-preview/recipe-preview.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UserInfoComponent,
    GreetingCreateComponent,
    FoodWasteComponent,
    FoodStorageComponent,
    SignupComponent,
    HomepageComponent,
    FuufuuComponent,
    ImageComponent,
    RecipeListComponent,
    CreateRecipeComponent,
    ViewRecipeComponent,
    SearchComponent,
    RecipePreviewComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
