import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../../service/security.service';
import { User } from '../user';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  user: User|null = null;

  constructor(private security: SecurityService) { }

  ngOnInit() {
    this.security.getUser().subscribe(user => this.user = user);
  }

  logout() {
    this.security.logout();
  }
/*
  deleteUser(user: User): void {
    this.userService.deleteUser(user)
      .subscribe( data => {
        this.users = this.users.filter(u => u !== user);
      })
  }; */
}


