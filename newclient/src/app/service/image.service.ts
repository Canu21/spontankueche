import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, share } from 'rxjs/operators';
import { Image } from '../image/image';



@Injectable({
  providedIn: 'root'
})
export class Imageservice {

  private images$ = new BehaviorSubject<Image[]>([]);

  constructor(private http: HttpClient) { }



  public saveImage(image: Image) {

    const result$ = this.http.post('/api/saveImage', image).pipe(share());

    result$.subscribe(() => {
      console.log('image saved');
    });
  }

  public readAllImages(): Observable<Image[]> {
    console.log('readAllImages()');
    this.http.get<Image[]>('/api/readAllImages').subscribe(
      image => this.images$.next(image)
    );
    return this.images$;
  }


}
