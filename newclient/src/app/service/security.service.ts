import { Injectable } from '@angular/core';
import { User } from '../security/user';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { share } from 'rxjs/operators';
import { newUser } from '../security/newuser';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private user$ = new BehaviorSubject<User | null>(null);

  constructor(private http: HttpClient) {
    this.http.get<User | null>('/api/sessionUser').subscribe(
      user => this.user$.next(user),
    );
  }

  public getUser(): Observable<User | null> {
    return this.user$;
  }

  public login(username: string, password: string): Observable<User> {

    const headers = new HttpHeaders({
      authorization: 'Basic ' + btoa(username + ':' + password),

      'X-Requested-With': 'XMLHttpRequest',
    });

    const login$ = this.http.get<User>('api/sessionUser', { headers }).pipe(share());


    login$.subscribe(
      user => this.user$.next(user),
      err => this.user$.next(null),
    );

    return login$;
  }
  /* Evtl. fuer spaeter !
 deleteUser(user) {
    return this.http.delete(this.userUrl + "/"+ user.id);
  }*/

  registerUser(newusername: string, newpassword: string, newemail: string) {
    const body: newUser = {
      username: newusername,
      userEmail: newemail,
      password: newpassword,

    };
    return this.http.post('/api/signup', body);
  }





  logout() {
    const logout$ = this.http.post('/api/logout', {}).pipe(share());

    logout$.subscribe(() => this.user$.next(null));

    return logout$;
  }

}
