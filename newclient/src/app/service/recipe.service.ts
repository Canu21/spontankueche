import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Recipe } from '../recipe/recipe';
import { Page } from '../page';
import { SearchResult } from '../search-result';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { Image } from '../image/image';


@Injectable({
  providedIn: 'root'
})
export class RecipeService {




  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Page<Recipe>>('/api/recipes');
  }
  search(name: string) {
    return this.http.get<SearchResult>('/api/recipes/' + encodeURIComponent(name));
  }
  createRecipe(body: Recipe) {
    return this.http.post('/api/createrecipe', body);
  }
  viewRecipe(recipeid: string) {
    return this.http.get<Recipe>('/api/recipe/' + encodeURIComponent(recipeid));
  }
  getIdForImage() {
    return this.http.get('/api/getrecipeid');
  }

}

