export interface Ingredient {

name: string;
quantity: string;
measure: string;
}
