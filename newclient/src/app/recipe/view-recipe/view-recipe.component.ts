import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from '../recipe';
import { Image } from '../../image/image';
import { RecipeService } from '../../service/recipe.service';
import { Imageservice } from '../../service/image.service';


@Component({
  selector: 'app-view-recipe',
  templateUrl: './view-recipe.component.html',
  styleUrls: ['./view-recipe.component.css']
})
export class ViewRecipeComponent implements OnInit {

  id: string;

  recipes: Recipe[] = [];

  recipe: Recipe;
  image: Image;


  constructor(private activatedRoute: ActivatedRoute, private recipeservice: RecipeService, private imageservice: Imageservice) { }

  ngOnInit() {
      this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
      this.recipeservice.viewRecipe(this.id).subscribe(
        data => {
          this.recipe = data;
        } );
  }

}
