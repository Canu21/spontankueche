import { Ingredient } from '../recipe/ingredient';

export interface Ingredient {
    name: string;
    quantity: string;
    measure: string;
}

export interface Recipe {
    id?: number;
    title: string;
    instruction: string;
    preparationTime?: string;
    ingredients: Ingredient[];
}




