import { Component, OnInit } from '@angular/core';
import { Recipe, Ingredient } from '../recipe';
import { Image } from '../../image/image';
import { Imageservice } from '../../service/image.service';
import { RecipeService } from '../../service/recipe.service';



@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.css']
})
export class CreateRecipeComponent implements OnInit {
  error: string|null = null;

  preparationTime = ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100', '110', '120'];

  model: Recipe = {
    title: '',
    ingredients: [],
    instruction: '',
    preparationTime: this.preparationTime[2],
  };

  submitted = false;

  file: File = new File([], '');
  image = {
    name: '',
    contentType: '',
    data: '',
  };

  images: Image[] = [ { name: 'name', contentType: '', data: ''}];


  constructor(private imageservice: Imageservice, private recipeservice: RecipeService) { }

  ngOnInit() {
  }

  onSubmit() {
   this.submitted = true;
  }


  // Rezeptbild Auswahl
  onFileChange(event: any) {
    console.log('onFileChange()');

    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];
      const img = document.querySelector('#preview img') as HTMLImageElement;
      // img.file = this.file;

      const reader = new FileReader();
      reader.onload = (e) => {
          img.src = reader.result.toString();
      };
      reader.readAsDataURL(this.file);
    }





  }
  // Rezeptbild upload
  onUpload() {

    this.image.name = this.file.name;
    this.image.contentType = this.file.type;
    const reader = new FileReader();
    reader.onload = (e) => {
      this.image.data = reader.result.toString();
    };
    reader.readAsDataURL(this.file);


    this.imageservice.saveImage(this.image);
    console.log('onUpload()');

  }


    createRecipe() {
    const reader = new FileReader();
    reader.onload = (e) => {
      this.image.data = reader.result.toString();
    };
    reader.readAsDataURL(this.file);

    this.recipeservice.createRecipe(this.model).subscribe(
      data => console.log(this.model)
    );

    this.image.name = this.file.name;
    this.image.contentType = this.file.type;


    this.imageservice.saveImage(this.image);
    console.log('onUpload()');
    }



  addIngredient() {
    this.model.ingredients.push({
      name: '',
      quantity: '',
      measure: '',
    });
  }

  deleteIngredient(ingredient: Ingredient) {
    this.model.ingredients.splice(this.model.ingredients.indexOf(ingredient), 1);
  }

  // entfernen
 // get diagnostic() { return JSON.stringify(this.model); }

}
