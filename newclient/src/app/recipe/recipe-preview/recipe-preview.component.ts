import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe';
import { Image } from '../../image/image';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from '../../service/recipe.service';
import { Imageservice } from '../../service/image.service';

@Component({
  selector: 'app-recipe-preview',
  templateUrl: './recipe-preview.component.html',
  styleUrls: ['./recipe-preview.component.css']
})
export class RecipePreviewComponent implements OnInit {
  id: string;

  recipes: Recipe[] = [];

  recipe: Recipe;
  image: Image;

  constructor(private activatedRoute: ActivatedRoute, private recipeservice: RecipeService, private imageservice: Imageservice) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.recipeservice.viewRecipe(this.id).subscribe(
      data => {
        this.recipe = data;
      } );

  }

}
