import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/service/recipe.service';
import { Recipe } from '../recipe';


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [];

  recipe = {
    title: '',
    instruction: ''
  };


  constructor(private _recipeService: RecipeService) { }

  submit2() {
    this._recipeService.getAll().subscribe(
      data => {
        this.recipes = data.content;
      },
      error => console.log(error)
    );
  }

  ngOnInit() {
    this._recipeService.getAll().subscribe(
      data => {
        this.recipes = data.content;
      },
      error => console.log(error)
    );
  }


}

