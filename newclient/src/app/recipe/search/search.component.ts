import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/service/recipe.service';
import { Recipe } from '../recipe';
import { Ingredient } from '../ingredient';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  recipes: Recipe[] = [];
  recipes2: Recipe[] = [];

  recipe = {
    title: '',
    instruction: '',
    Ingredient: [],
  };
  name = '';

  constructor(private _recipeService: RecipeService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.name = this.activatedRoute.snapshot.paramMap.get('name') || '';
    this.submit3();
  }

  submit3() {
    this._recipeService.search(this.name).subscribe(
      data => {
        this.recipes = data.topResults;
        this.recipes2 = data.otherResults;
      },
      error => console.log(error)
    );
  }

}
