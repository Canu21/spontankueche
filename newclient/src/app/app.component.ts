import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Greeting } from './greeting/greeting';

import { User } from './security/user';
import { GreetingService } from './service/greeting.service';
import { SecurityService } from './service/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'client';
  greetings: Greeting[]|null = null;
  user: User|null = null;

  constructor(private greetingService: GreetingService, private security: SecurityService) {
  }

  ngOnInit(): void {
    this.greetingService.getGreetings().subscribe(
      greetings => this.greetings = greetings
    );
    this.security.getUser().subscribe(
      user => this.user = user
    );
  }
}
