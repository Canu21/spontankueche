import { Recipe } from './recipe/recipe';

export interface SearchResult {
    topResults: Recipe[];
    otherResults: Recipe[];
}
