import { Component, OnInit } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { SecurityService } from '../service/security.service';
import { newUser } from '../security/newuser';
import { NgForm, Form } from '@angular/forms';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  error: string|null = null;
  newusername = '';
  newpassword = '';
  newemail = '';

    constructor(private security: SecurityService) {

    }

  ngOnInit() {
  // this.resetForm();
  }
/*

    if (form != null)
      form.reset();
    this.newUser = {
      UserName: '',
      Password: '',
      Email: '',

    }
  }
 */
signup() {
  this.security.registerUser(this.newusername, this.newpassword, this.newemail).subscribe({
    complete: () => this.error = null,
    error: err => this.error = err,

});
}

}

