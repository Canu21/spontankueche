import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { GreetingCreateComponent } from './greeting/greeting-create/greeting-create.component';
import { FuufuuComponent } from './fuufuu/fuufuu.component';
import { RecipeListComponent } from './recipe/recipe-list/recipe-list.component';

import { FoodWasteComponent } from './food-waste/food-waste.component';
import { FoodStorageComponent } from './food-storage/food-storage.component';
import { SearchComponent } from './recipe/search/search.component';
import { SignupComponent } from './signup/signup.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { CreateRecipeComponent } from './recipe/create-recipe/create-recipe.component';
import { ViewRecipeComponent } from './recipe/view-recipe/view-recipe.component';



const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'greeting', component: GreetingCreateComponent },
  { path: 'recipes', component: RecipeListComponent},
  { path: 'fuu', component: FuufuuComponent },
  { path: 'create-recipe', component: CreateRecipeComponent },
  { path: 'view-recipe/:id', component: ViewRecipeComponent},
  { path: 'food-waste', component: FoodWasteComponent},
  { path: 'food-storage', component: FoodStorageComponent},
  { path: 'search/:name', component: SearchComponent},
  { path: 'search', component: SearchComponent},
  { path: 'signup' , component: SignupComponent},
  { path: 'login' , component: LoginFormComponent},
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],

})
export class AppRoutingModule {}
